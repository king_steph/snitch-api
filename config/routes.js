/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/
 /*
     * Organization routes
     */

    'POST /snitch/organization': {
      controller: 'OrganizationController',
      action: 'create',
    },
    'GET /snitch/organization/:id': {
      controller: 'OrganizationController',
      action: 'read',
    },
    'GET /snitch/organization': {
      controller: 'OrganizationController',
      action: 'list',
    },
    'PATCH /snitch/organization/:id': {
      controller: 'OrganizationController',
      action: 'update',
    },
    'DELETE /snitch/organization/:id': {
      controller: 'OrganizationController',
      action: 'delete',
    },
  
    /*
     * User routes
     */

    'POST /snitch/user': {
      controller: 'UserController',
      action: 'create',
    },
    'GET /snitch/user/:id': {
      controller: 'UserController',
      action: 'read',
    },
    'GET /snitch/user': {
      controller: 'UserController',
      action: 'list',
    },
    'PATCH /snitch/user/:id': {
      controller: 'UserController',
      action: 'update',
    },
    'DELETE /snitch/user/:id': {
      controller: 'UserController',
      action: 'delete',
    },

    /*
     * Tag routes
     */

    'POST /snitch/tag': {
      controller: 'TagController',
      action: 'create',
    },
    'GET /snitch/tag/:id': {
      controller: 'TagController',
      action: 'read',
    },
    'GET /snitch/tag': {
      controller: 'TagController',
      action: 'list',
    },
    'PATCH /snitch/tag/:id': {
      controller: 'TagController',
      action: 'update',
    },
    'DELETE /snitch/tag/:id': {
      controller: 'TagController',
      action: 'delete',
    },
    /*
     * Comment routes
     */

    'POST /snitch/comment': {
      controller: 'CommentController',
      action: 'create',
    },
    'GET /snitch/comment/:id': {
      controller: 'CommentController',
      action: 'read',
    },
    'GET /snitch/comment': {
      controller: 'CommentController',
      action: 'list',
    },
    'PATCH /snitch/comment/:id': {
      controller: 'CommentController',
      action: 'update',
    },
    'DELETE /snitch/comment/:id': {
      controller: 'CommentController',
      action: 'delete',
    },

};
