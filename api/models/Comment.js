/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        comments :{
            type: 'string',
        },
        organization: {
            model: 'organization',
            required: true,
        },
        user: {
            model : 'user',
            required: true,
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false,
        }
    },
    connection : 'mongodb',
};

