/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        username : {
            type: 'string',
            required: true,
        },
        password :{
            type: 'string',
        },
        image: {
            type: 'json',
        },
        comments: {
            collection: 'comment',
            via: 'user',
        },
        organization: {
            collection : 'organization',
            via: 'user',
        },
        location: {
            type: 'json',
            defaultsTo: {
                type: 'Point',
                coordinates: [3.3769997, 6.4455528], // MongoDB format [lon,lat]
            },
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false,
        }
    },
    connection : 'mongodb',
};

