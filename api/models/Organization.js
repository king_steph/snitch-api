/**
 * Organization.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        username :{
            type: 'string',
            required: true,
        },
        password: {
            type: 'string',
            required: true,
        },
        rating: {
            type: 'integer',
            defaultsTo: 0,
        },
        activity: {
            type: 'integer',
            defaultsTo: 0,
        },
        description: {
            type: 'string',
        },
        mobile: {
            type: 'number',
        },
        email : {
            type: 'email',
            unique: true,
        },
        comments: {
            collection: 'comment',
            via: 'organization',
        },
        tags: {
            collection : 'tag',
            via: 'organization',
        },
        user: {
            collection: 'user',
            via: 'organization',
        },
        image: {
            type: 'json',
        },
        location: {
            type: 'json',
            defaultsTo: {
                type: 'Point',
                coordinates: [3.3769997, 6.4455528], // MongoDB format [lon,lat]
            },
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false,
        }
    },
    connection : 'mongodb',
};

