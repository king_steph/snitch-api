/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: async(req, res) => {
        try {
            const data = req.body;
            const { username } = data;
            const organization = await Organization.findOne({ username,  isDeleted: false });
            if(organization) return res.json(500, { message: `${username} already exists` });
            const user = await User.create(data);
            if (!user) return res.json(500, { message: 'The user was not created' });

            return res.json(201, {message: `${user.username} has been created successfully`, data: user});

        } catch(err) { return res.json(400, { message: err }) };      
    },

    read: async(req, res) => {
        try {
            const criteria = {
                id : req.params.id,
                isDeleted: false,
            }
            const populatable = ['comments', 'organization'];
    
            const userData = await User.findOne(criteria).populate(populatable);
            if (!userData) return res.json(500, { message: 'The user does not exist' });
            return res.json(200, { meassage: 'Data retrived for user', data: userData });

        }catch(err) { return res.json(400, { message: err }) };
        
    },

    list: async (req, res) => {
        try {
            const records = await User.find({ isDeleted: false });

            if(!records) { return res.json(400, { message: 'User was not found'}) }

            return res.json(200, { message: 'data retrieved successfully' , data: records })

        }catch(err) {
            res.json(400, { message: err });
        }

    },
    update: async(req, res) => {
        try {
            const data = req.body;
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
            };

            const updatedRecord = await User.update(criteria, data);
            if (!updatedRecord) return res.json(500, { message: 'User was not updated successfully'});
            return res.json(200, {message: 'User was updated successfully', data: updatedRecord })

        }catch(err) { return res.json(400, { message: err }) };
        
    },

    delete: async(req, res) => {
        try{
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
             };
    
            const deletedRecord = await User.update(criteria, {isDeleted: true });
            if (!deletedRecord) return res.json(500, { message: 'User was not deleted successfully'});
            return res.json(200, {message: 'User was deleted successfully', data: deletedRecord })

        }catch (err) { return res.json(400, { message: err }) };
       
    }
};
