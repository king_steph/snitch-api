/**
 * TagController
 *
 * @description :: Server-side logic for managing tags
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: async(req, res) => {
        try{
            const data = req.body;
            const tag = await Tag.create(data);
            if (!tag) return res.json(500, { message: 'The tag was not created' });

            return res.json(201, {message: `${tag.username} has been created successfully`, data: tag});

        }catch (err) { return res.json(400, { message: err }) };
        
    },

    read: async(req, res) => {
        try{
            const criteria = {
                id : req.params.id,
                isDeleted: false,
            }
            const populatable = ['comments', 'organization'];
    
            const tagData = await Tag.findOne(criteria).populate(populatable);
            if (!tagData) return res.json(500, { message: 'The Tag does not exist' });
            return res.json(200, { meassage: 'Data retrived for Tag', data: tagData });

        }catch (err) { return res.json(400, { message: err }) };
       
    },

    update: async(req, res) => {
        try {
            const data = req.body;
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
             };
    
            const updatedRecord = await Tag.update(criteria, data);
            if (!updatedRecord) return res.json(500, { message: 'Tag was not updated successfully'});
            return res.json(200, {message: 'Tag was updated successfully', data: updatedRecord })
        } catch (err) { return res.json(400, { message: err }) };
       
    },
    list: async (req, res) => {
        try {
            const records = await User.find({ isDeleted: false });

            if(!records) { return res.json(400, { message: 'User was not found'}) }

            return res.json(200, { message: 'data retrieved successfully' , data: records })

        }catch(err) {
            res.json(400, { message: err });
        }

    },

    delete: async(req, res) => {
        try{
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
             };
    
            const deletedRecord = await Tag.update(criteria, {isDeleted: true });
            if (!deletedRecord) return res.json(500, { message: 'Tag was not deleted successfully'});
            return res.json(200, {message: 'Tag was deleted successfully', data: deletedRecord })

        } catch (err) { return res.json(400, { message: err }) };
        
    }
    
	
};

