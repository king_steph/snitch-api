/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: async(req, res) => {
        try {
            const data = req.body;
            const comment = await Comment.create(data);
            if (!comment) return res.json(500, { message: 'The comment was not created' });

            return res.json(201, {message: `${tag.username} has been created successfully`, data: tag});

        } catch (err) { return res.json(400, { message: err }) };
        
    },

    read: async(req, res) => {
        try {
            const criteria = {
                id : req.params.id,
                isDeleted: false,
            }
            const populatable = ['comments', 'organization'];
    
            const comData = await Comment.findOne(criteria).populate(populatable);
            if (!comData) return res.json(500, { message: 'The Comment does not exist' });
            return res.json(200, { meassage: 'Data retrived for Comment', data: comData });
        } catch (err) { return res.json(400, { message: err }) };
        
    },
    list: async (req, res) => {
        try {
            const records = await User.find({ isDeleted: false });

            if(!records) { return res.json(400, { message: 'User was not found'}) }

            return res.json(200, { message: 'data retrieved successfully' , data: records })

        }catch(err) {
            res.json(400, { message: err });
        }

    },

    update: async(req, res) => {
        try {
            const data = req.body;
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
            };

            const updatedRecord = await Comment.update(criteria, data);
            if (!updatedRecord) return res.json(500, { message: 'Comment was not updated successfully'});
            return res.json(200, {message: 'Comment was updated successfully', data: updatedRecord });
        } catch (err) { return res.json(400, { message: err }) };
        
    },

    delete: async(req, res) => {
        try{
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
             };
    
            const deletedRecord = await Comment.update(criteria, {isDeleted: true });
            if (!deletedRecord) return res.json(500, { message: 'Comment was not deleted successfully'});
            return res.json(200, {message: 'Comment was deleted successfully', data: deletedRecord });
        } catch (err) { return res.json(400, { message: err }) };
        
    }
	
};

