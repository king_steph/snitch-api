/**
 * OrganizationController
 *
 * @description :: Server-side logic for managing organizations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: async(req, res) => {
        try {
            const data = req.body;
            const { username } = data;
            const user = await User.findOne({ username, isDeleted: false});
            if(user) return res.json(500, { message: `${username} already exists` });
            const organization = await Organization.create(data);
            if (!organization) return res.json(500, { message: 'The Organization was not created' });

            return res.json(201, {message: `${organization.username} has been created successfully`, data: organization});

        } catch (err) { return res.json(400, { message: err }) };
        
    },

    read: async(req, res) => {
        try {
            const criteria = {
                id : req.params.id,
                isDeleted: false,
            }
            const populatable = ['comments', 'organization'];
            
            const orgData = await Organization.findOne(criteria).populate(populatable);
            if (!orgData) return res.json(400, { answer: 'The organization does not exist' });
            return res.json(200, { message: 'Data retrived for organization', data: orgData });

        } catch (err) { return res.json(400, { message: err }) };
        
    },
    list: async (req, res) => {
        try {
            const records = await User.find({ isDeleted: false });

            if(!records) { return res.json(400, { message: 'User was not found'}) }

            return res.json(200, { message: 'data retrieved successfully' , data: records })

        }catch(err) {
            res.json(400, { message: err });
        }

    },
    update: async(req, res) => {
        try {
            const data = req.body;
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
            };

            const updatedRecord = await Organization.update(criteria, data);
            if (!updatedRecord) return res.json(500, { message: 'Organization was not updated successfully'});
            return res.json(200, {message: 'Organization was updated successfully', data: updatedRecord });
        } catch (err) { return res.json(400, { message: err }) };

    },
        

    delete: async(req, res) => {
        try {
            const criteria = { 
                id: req.params.id, 
                isDeleted: false,
             };
    
            const deletedRecord = await Organization.update(criteria, {isDeleted: true });
            if (!deletedRecord) return res.json(500, { message: 'Organization was not deleted successfully'});
            return res.json(200, {message: 'Organization was deleted successfully', data: deletedRecord });
        } catch (err) { return res.json(400, { message: err }) };
        
    }
	
};

